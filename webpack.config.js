
const path = require('path')
const webpack = require('webpack')

const config = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'pulqui.js'
  },
  module: {
    resolve: {
      root: [
        path.resolve('./src'),
        path.resolve('./components'),
      ]
    },
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel' }
    ]
  },
  devServer: {
    contentBase: 'dist'
  },
  plugins: [
    new webpack.ProvidePlugin({
      h: path.resolve('./node_modules/universal-components/src/bel-create-element')
    })
  ]
}

module.exports = config
