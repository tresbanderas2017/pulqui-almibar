import yo from 'yo-yo'
import nanobus from 'nanobus'
import nanorouter from 'nanorouter'
import nanohref from 'nanohref'

const bus = nanobus()
const router = nanorouter()

function Pulqui() {
  if (!(this instanceof Pulqui)) return new Pulqui()
  this.postRender = null
	this.state = null
}

Pulqui.prototype.iniciar =  function(container) {
  document.body.appendChild(container)
}

Pulqui.prototype.getRoute = function() {
  const route = window.location.pathname + window.location.hash
	return router(route)
}

Pulqui.prototype.start = function(template, selector) {
  const routeTemplate = this.getRoute()

  document.body.appendChild(template)

  const pulquiPlace = document.getElementById(selector)

  const pulquiContent = document.createElement('div')
  pulquiContent.appendChild(routeTemplate)
  pulquiContent.id = selector

  yo.update(pulquiPlace, pulquiContent)

  bus.prependListener('pushState', function(href) {
    window.history.pushState({}, null, href)
    bus.emit('render')
  })

  bus.prependListener('popState', function() {
    bus.emit('render')
  })

  window.addEventListener('popstate', function(e) {
    bus.emit('popState')
  })

  bus.prependListener('render', () => {
    const currentRoute = window.location.pathname + window.location.hash
    const tree = router(currentRoute)

    const pulquiPlace = document.getElementById(selector)
    const pulquiContent = document.createElement('div')
    pulquiContent.appendChild(tree)
    pulquiContent.id = selector

    yo.update(pulquiPlace, pulquiContent)
    this.postRender()
  })

}

nanohref(function (location) {
	var href = location.href
	var currHref = window.location.href

	if (href === currHref) return
	bus.emit('pushState', href)
})


export {
  router,
  Pulqui
}
