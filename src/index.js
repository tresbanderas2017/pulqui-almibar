import { router, Pulqui } from '../'
import Home from './componentes/home/Home.js'

import Ayuda from './componentes/ayuda/Ayuda.js'

import TemplateInicial from './componentes/inicial/TemplateInicial.js'

router.on('/#ayuda', (params) => <Ayuda />)
router.on('/', (params) => <Home />)


var templateInicial = <TemplateInicial /> 
/*  <div>
    <h1>Layout</h1>
    <a href="/"> Home</a>
    <a href="/#ayuda"> Ayuda</a>    
    <div id="pulqui-contenedor"></div>    
  </div>
*/

var miPulquiApp = Pulqui()
miPulquiApp.start(templateInicial, "pulqui-contenedor")
miPulquiApp.postRender = function() {}
