import yo from 'yo-yo'
import Lista from './Lista.js'
import Buscador from './Buscador.js'
import Agregador from './Agregador.js'

function Home(){

	function handlerInput(event){
		console.log("estan apretando una tecla");
		var valor = event.target.value	
		console.log(valor);				

		estado.listaActual = valor?estado.listaOriginal.filter(function(elem){
		  	return (elem.nombre.indexOf(valor)!=-1)
		  }		
		):
		estado.listaOriginal;

		console.log(estado.listaActual.length);
		var componenteAct = render(estado);
		console.log(componenteAct);
		yo.update(componente, componenteAct);
	}

	function nameInput(event){	
		console.log("nombre contacto nuevo");
		var valor = event.target.value;
		estado.nuevoNombre = valor;
		console.log(valor);	
		
	}
	function telInput(event){	
		console.log("telefono contacto nuevo");
		var valor = event.target.value;	
		estado.nuevoTelefono = valor;
		console.log(valor);	
		
	}	
	function clickInput(event){	
		console.log("esta agregando un contacto nuevo");
		var ultimo = estado.listaOriginal[estado.listaOriginal.length-1];		
		var nuevo = {id:ultimo.id+1, nombre: estado.nuevoNombre, telefono:estado.nuevoTelefono, eliminado:false}
		estado.listaOriginal.push(nuevo)
		console.log(nuevo);
		var componenteAct = render(estado);
		//console.log(componenteAct);
		yo.update(componente, componenteAct);
		
	}
	function deleteInput(id){
		console.log(id);
		estado.listaActual = estado.listaOriginal.map(elem=>{
			var ref = (elem.id===id?Object.assign({},elem,{eliminado:true}):elem);
			return ref;		
		})
		estado.listaOriginal = estado.listaActual;
		console.log(estado.listaActual);
		var componenteAct = render(estado);
		yo.update(componente, componenteAct);
		 
	}

	function render	(estado){
		return (
			<div id="home">		
				<h2>Agenda  <i class="material-icons">contact_phone</i></h2>
				<div id="buscador"><Buscador handlerInput={handlerInput}/></div>
				<div id="lista"><Lista arg={estado.listaActual} arg1 = {deleteInput}/></div>
				<div id="nuevoContacto"><Agregador clickInput={clickInput} nameInput={nameInput} telInput={telInput}/></div>
			</div>
		);
	}
        var estado = {
		listaActual:[],
		listaOriginal: [
			{
			id:1,			
			nombre:"Perez Juan",
			telefono:"123451",
			eliminado:false
			},
			{
			id:2,
			nombre:"Perez Maria",
			telefono:"123452",
			eliminado:false
			},
			{
			id:3,
			nombre:"Gonzalez Pedro",
			telefono:"123453",
			eliminado:false
			},
			{
			id:4,
			nombre:"Garcia Juan",
			telefono:"123454",
			eliminado:false
			}
		       ],
		nuevoNombre:"",
		nuevoTelefono:""
	}
	estado.listaActual = estado.listaOriginal;
        var componente = render(estado);
	return componente;
}

export default Home;
